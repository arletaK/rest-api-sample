# **Instrukcja** #

## Wymagania ##
Visual Studio 2015

## Uruchomienie ##
Otworzyć projekt i go uruchomić

## Info ##
Content-type: application/x-www-form-urlencoded

Name=Imie&Nazwisko=D&Index=12345&City=Lodz&Id=7d7cb833-b66-4f75-8efd-3850ca17cd2d

* /api/Students (GET) => Lista wszystkich studentów
* /api/Students/{id} (GET) => Pobranie studenta o podanym id
* /api/Students/{id} (DELETE) => Usunięcie studenta o podanym id
* /api/Students (POST) => Dodanie studenta
* /api/Students (PUT) => Aktualizacja studentów