﻿using RestApiSample.Data;
using RestApiSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace RestApiSample.Controllers
{
    public class StudentsController : ApiController
    {
        public IEnumerable<Student> Get()
        {
            return Database.Students;
        }

        public Student Get(string id)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));

            return Database.Get(id);
        }

        public void Post([FromBody]Student student)
        {
            if (student == null) throw new ArgumentNullException(nameof(student));

            Database.Add(student);
        }

        public void Put([FromBody]Student student)
        {
            if (student == null) throw new ArgumentNullException(nameof(student));

            Database.Update(student);
        }

        public void Delete(string id)
        {
            if (id == null) throw new ArgumentNullException(nameof(id));


            Database.Delete(id);
        }
    }
}
