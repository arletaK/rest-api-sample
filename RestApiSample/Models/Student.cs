﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApiSample.Models
{
    public class Student
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Lastname { get; set; }
        public int Index { get; set; }
        public string City { get; set; }

        public Student()
        {
            Id = Guid.NewGuid().ToString();
        }
    }
}