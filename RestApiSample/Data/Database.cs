﻿using RestApiSample.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RestApiSample.Data
{
    public class Database
    {
        private static List<Student> students = new List<Student>(new[] 
        {
            new Student() {Name="Ala", Lastname="Kowalska", City="Lodz", Index=1234},
            new Student() {Name="Ola", Lastname="Nowak", City="Warszawa", Index=5678},
            new Student() {Name="Piotr", Lastname="Kowal", City="Lodz", Index=1122},
        });

        public static List<Student> Students
        {
            get
            {
                return new List<Student>(students);
            }
        }

        public static void Add(Student student)
        {
            if (Students.Where(x => x.Id == student.Id).Any())
            {
                throw new Exception($"User with Id = {student.Id} exist.");
            }

            students.Add(student);
        }

        public static void Delete(string id)
        {
            var studentToDelete = students.Where(x => x.Id == id).SingleOrDefault();

            if (studentToDelete == null)
            {
                throw new Exception($"User with Id = {id} not exist.");
            }

            students.Remove(studentToDelete);
        }

        public static void Update(Student student)
        {
            var studentToUpdate = students.Where(x => x.Id == student.Id).SingleOrDefault();

            if (studentToUpdate == null)
            {
                throw new Exception($"User with Id = {student.Id} not exist.");
            }

            students.Remove(studentToUpdate);
            students.Add(student);
        }

        public static Student Get(string id)
        {
            return students.Where(x => x.Id == id).SingleOrDefault();
        }
    }
}